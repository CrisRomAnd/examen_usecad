$(document).ready(function () {
    $('body').loadingModal({
        animation: 'doubleBounce'
    });


    function get_materias() {
        return ajax_request("controller/get_materias.php", "GET", {});
    }

    function get_materias_lab() {
        return ajax_request("controller/get_materias_lab.php", "GET", {});
    }

    function get_materias_seriadas() {
        return ajax_request("controller/get_materias_seriadas.php", "GET", {});
    }

    function get_promedios_por_materia() {
        return ajax_request("controller/get_promedios_por_materia.php", "GET", {});
    }



    function load_panel_materia2() {
        set_active_class(this);

        $('body').loadingModal({
            animation: 'doubleBounce'
        });
        $(".title-head").text("Inventario - Materia");
        $(".titulo-h1").text("Materia");
        $(".main-panel").load("view/materias.html", function () {
            //usuario_id = sessionStorage.getItem("id");
            //servicios_renta = get_servicios_renta_usuario(usuario_id);
            provs = get_materias();
            prov= []
            provs.forEach(element => {
                element.option = "<button class='btn-del-prov btn btn-success btn-block' id=" + element.id + ">Eliminar</button>"
                prov.push(element);
            });

            console.log(prov);
            if (prov != []) {
                $("#table-mat").DataTable({
		    //destroy: true,
		    buttons:[
			{extend: 'searchPanes', cssRules:{}}
		    ],
		    dom: 'Bfrtip',
                    data: prov,
                    columns: [
                        {data: "id"},
                        {data: "clave"},
                        {data: "nombre_materia"},
                        {data: "laboratorio_edo"},
                        {data: "horas_teoricas"},
                        {data: "horas_practicas"},
                        {data: "creditos"},
                        {data: "semestre"},
                        {data: "seriada_con"},
                        {data: "division_materia"},
                        {data: "campo_pertenesiente"}
                    ],
                    deferRender: true
                });
		
            } else {
                $("#table-mat").DataTable({
                    destroy: true
                });
            }
        });
        $('body').loadingModal('destroy');
        return null;
    }

    function load_panel_lab() {
        $('body').loadingModal({
            position: 'auto',
            text: '',
            color: '#fff',
            opacity: '0.7',
            backgroundColor: 'rgb(0,0,0)',
            animation: 'doubleBounce'
        });

        set_active_class(this);
        $(".title-head").text("Materia Labaoratorio");
        $(".titulo-h1").text("Materia");
        $(".main-panel").load("view/materias_lab.html", function () {
            //usuario_id = sessionStorage.getItem("id");
            //servicios_renta = get_servicios_renta_usuario(usuario_id);
            provs = get_materias_lab();
            prov= []
            provs.forEach(element => {
                element.option = "<button class='btn-del-prov btn btn-success btn-block' id=" + element.id + ">Eliminar</button>"
                prov.push(element);
            });

            if (prov != []) {
                $("#table-mat-lab").DataTable({
                    destroy: true,
                    data: prov,
                    columns: [
                        {data: "id"},
                        {data: "clave"},
                        {data: "nombre_materia"},
                        {data: "laboratorio_edo"},
                        {data: "creditos"},
                        {data: "semestre"},
                        {data: "horas_practicas"},
                        {data: "horas_teoricas"},
                        {data: "division_materia"},
                        {data: "campo"}
                    ]
                });
            } else {
                $("#table-mat-lab").DataTable({
                    destroy: true
                });
            }
        });
        // $('.dt-button-collection').css({"width": "700px", "top": "43.3167px", "left": "15px"});

        $('body').loadingModal('destroy');
    }

    function load_panel_seriadas() {
        $('body').loadingModal({
            position: 'auto',
            text: '',
            color: '#fff',
            opacity: '0.7',
            backgroundColor: 'rgb(0,0,0)',
            animation: 'doubleBounce'
        });
        set_active_class(this);
        $(".title-head").text("Promedio de Asignaturas");
        $(".titulo-h1").text("Promedio de Asignaturas");
        $(".main-panel").load("view/materias_seriadas.html", function () {
            //usuario_id = sessionStorage.getItem("id");
            //servicios_renta = get_servicios_renta_usuario(usuario_id);
            provs = get_materias_seriadas();
            prov= []
            provs.forEach(element => {
                element.option = "<button class='btn-del-prov btn btn-success btn-block' id=" + element.id + ">Eliminar</button>"
                prov.push(element);
            });

            if (prov != []) {
                $("#table-mat-seriadas").DataTable({
                    destroy: true,
                    data: prov,
                    columns: [
			{data:"id"},
			{data:"clave"},
			{data:"nombre"},
			{data:"laboratorio_edo"},
			{data:"horas_teoricas"},
			{data:"horas_practicas"},
			{data:"creditos"},
			{data:"semestre"},
			{data:"seriada_con"},
			{data:"division_materia"},
			{data:"campo_pertenesiente"}
		    ]
                });
            } else {
                $("#table-mat-seriadas").DataTable({
                    destroy: true
                });
            }
        });
        $('body').loadingModal('destroy');
    }

    function load_avance_por_materia(){
        $('body').loadingModal({
            animation: 'doubleBounce'
        });
	set_active_class(this);
	
        $(".title-head").text("Avance Promedio");
        $(".titulo-h1").text("Avance Promedio");
        $(".main-panel").load("view/avance_promedio.html", function () {});
	
	data = get_promedios_por_materia();
	
	clv = [];
	prom = [];
	for( i in data){
	    clv.push(data[i].clave);
	    prom.push(data[i].promedio);
	}

	
	var options = {
            series: [{
		name: 'Promedios',
		data: prom
            }],
            chart: {
		type: 'bar',
		height: 450,
		animations: {
		    enabled: false
		}
            },
            plotOptions: {
		bar: {
		    horizontal: false,
		    columnWidth: '40%',
		    // endingShape: 'rounded'
		},
            },
            dataLabels: {
		enabled: false
            },
            stroke: {
		show: true,
		colors: ['transparent'],
		width: 2
            },
	    
            xaxis: {
		categories: clv
            },
            yaxis: {
		title: {
		    text: 'Promedio'
		},
		labels: {
		    formatter: function (val) {
			return val.toFixed(2)
		    }
		}
            },
            fill: {
		opacity: 1
            },
            tooltip: {
		y: {
		    formatter: function (val) {
			return val + " pomedio"
		    }
		}
            }
        };

	var chart = new ApexCharts(document.querySelector("#container"), options);
	chart.render().then(() => {
            chart.dataURI().then(({ imgURI, blob }) => { //Here shows error
		var pdf = new jsPDF();
		pdf.addImage(imgURI, 'PNG', 0, 0);
		bootbox.alert("Descargando");
		pdf.save("Grafica.pdf");
            })})
        $('body').loadingModal('destroy');

    }

    function cerrar() {
        console.log("logout");
        var xhttp = new XMLHttpRequest();
        xhttp.open("GET", "controller/logout.php", false);
        xhttp.send();
        sessionStorage.clear();
        bootbox.alert("Cerrando Session");
        location.href = "index.php";
    }

    // $("#wrapper").on("click", ".btn-index", index);
    $("#wrapper").on("click", ".btn-avance-por-asignatura", load_avance_por_materia);
    $("#wrapper").on("click", ".btn-materias", load_panel_materia2);
    $("#wrapper").on("click", ".btn-materias-lab", load_panel_lab);
    $("#wrapper").on("click", ".btn-materias-seriadas", load_panel_seriadas);
    $("#wrapper").on("click", ".btn-logout", cerrar);

    $('body').loadingModal('destroy');

});
