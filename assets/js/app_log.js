$(document).ready(function(){

    $.fn.inputFilter = function(inputFilter) {
        return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
            if (inputFilter(this.value)) {
                this.oldValue = this.value;
                this.oldSelectionStart = this.selectionStart;
                this.oldSelectionEnd = this.selectionEnd;
            } else if (this.hasOwnProperty("oldValue")) {
                this.value = this.oldValue;
                this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
            } else {
                this.value = "";
            }
        });
    };

    $("#login_a").click(function(){
        $("#shadow").fadeIn("normal");
        $("#login_form").fadeIn("normal");
        $("#user_name").focus();
    });
    $("#cancel_hide").click(function(){
        $("#login_form").fadeOut("normal");
        $("#shadow").fadeOut();
    });

    $("#inputEmail").inputFilter(function(value) {
	return /^-?\d*$/.test(value); });


    $("#ingresar-btn").click(function(){


        username=$("#inputEmail").val();
        password=$("#inputPassword").val();
        console.log(password)
        console.log(username)
        $.ajax({
            type: "POST",
            url: "controller/login.php",
            data: "inputEmail="+username+"&inputPassword="+password,
            success: function(html){
                if(html=='true')
                {
                    location.reload();
                }
                else
                {
                    $("#add_err").html("Usuario o password incorrectos");
                }
            },

            beforeSend:function()
            {
                //$("#add_err").html("Cargando...")

            }
        }).done(function(resp){
            if(!resp.error){
                location.reload();
            }
            else{
                alert("El usuario indicado es incorrecto");
            }
        });
        return false;
    });
});


