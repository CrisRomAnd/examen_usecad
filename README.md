
  * # Examen

## Dependencias

* Postgres >= 11
* php >= 5 
  * Se recomienda >= 7.1
* Servidor Web como apache o nginx

## Objetivos

1. Frontend
  1. Implementar una vista haciendo uso de un framework de vista (Bootstrap, materialize, etc) [X]
    1. Preferentemente responsivo [X]
    2. Utilizar de CDN y recursos locales para descargar javascript y css de los paquetes [X]
  2. Implementar formularios dinámicos asíncronos de una sola página. [X]
    1. Validadores de información de lado de front end. (Saltar caracteres especiales o no permitir su entrada, tamaño máximo, etc.)[^1] [X] 
    2. Ventana de aviso de descarga personalizada durante la obtención de datos del servidor.[^2] [X]
    3. Representar de forma gráfica las calificaciones del servidor [X] ![graph](doc/img/1.png "Promedio de calificaciones según asignaturas")
  
    1. Crear gráficas haciendo uso de alguna biblioteca externa [X] 
    2. Descargar al menos un reporte con una gráfica en formato PDF [X] ![graphpdf](doc/img/1.png "Promedio de calificaciones según asignaturas")
    
2. Back End (PHP)
  1. Crear la conexión con la base de datos. [X] [PDO](model/Conexion_BD.php "conexión a bd")
  2. Crear scripts backend de procesamiento y peticiones de datos [controller](controller "controller")
    1. Hacer uso de validadores de seguridad [sesion](controller/login.php)
  3. Implementar estructuras de datos para los modelos de CRUD de las tablas de la base de datos [modelos_PDO](model)
  
3. Base de Datos
  1. Crear la base de datos en PostgreSQL 11. [crear_usuario_y_base_de_datos](model/sql/origin.sql)
  2. Basarse en el mapa curricular anexo para generar la base de datos en SQL. [](model/sql/origin.sql)
  3. Crear una forma de validar la seriación entre asignaturas. [X] ![basedatos](doc/img/3.png "Base de Datos")
  4. Asociar los campos de profundización con el plan de estudios de tronco común [X]
  5. Guardar las horas teóricas, prácticas y la suma de ambas para cada materia [X]
  6. Rutina para consultar las materias seriadas. [X]
  7. Rutina para consultar las materias por semestre. [X]
  8. Rutina para consultar las materias de la división específica. [X]
  9. Rutina para consultar las materias con laboratorios incluidos(L) y su vez los laboratorios por separado(L+) [X]
  

[materias](doc/img/1.gif "materias")


[^1]: La pagina de lógin, solo permite ingresar números en donde va el numero de cuenta.
[^2]: En cada pestaña esta implementada, sin embargo carga muy rápido.
