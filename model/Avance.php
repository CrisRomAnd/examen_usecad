<?php
class Avance{
    private $db;

    function __construct(){
        $this->db=new Conexion();
    }

    function get_all_avance(){
        $sql = "select m.clave, calificacion from avance_usuario inner join materias m on avance_usuario.materia_cursada = m.id";
        $stmt =  $this->db->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    function get_avance_promedios(){
        $sql = "select m.clave, avg(calificacion) as promedio from avance_usuario inner join materias m on avance_usuario.materia_cursada = m.id group by m.clave";
        $stmt =  $this->db->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    function get_clave_avance($clv){
        $sql = "select m.clave, calificacion from avance_usuario inner join materias m on avance_usuario.materia_cursada = m.id where clave=:clv";

        $stmt = $$this->db->prepare($sql);
        $stmt->bindParam(':clv', $clv);
        $stmt->execute();
        if($stmt == true){
        	$idReturn = $stmt->fetch(PDO::FETCH_ASSOC);
            return $idReturn;
        } else { 
            return false;
        }
    }
}
