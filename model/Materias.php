<?php
class Materias{
    private $db;

    function __construct(){
        $this->db=new Conexion();
    }

    function ver_materias(){
        $sql  = "SELECT m.id, m.clave,m.nombre  as nombre_materia,m.laboratorio_edo,m.horas_teoricas,m.horas_practicas,m.creditos,m.semestre, ms.nombre AS seriada_con,dm.division_materia,  c.nombre as campo_pertenesiente FROM materias m LEFT JOIN campo c ON m.pertenece_a_campo = c.id LEFT JOIN division_materia dm ON m.tipo_materia = dm.id LEFT JOIN materias ms ON ms .id = m.materia_seriada ORDER BY m.id ASC";
        $stmt = $this->db->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    function ver_laboratorios(){
        $sql = "select m.id,m.clave,m.nombre as nombre_materia,m.laboratorio_edo,m.creditos,m.semestre,m.horas_practicas,m.horas_teoricas,dm.division_materia,c.nombre as campo from materias m inner join campo c on m.pertenece_a_campo = c.id inner join division_materia dm on m.tipo_materia = dm.id where m.laboratorio_edo is not null";
        $stmt = $this->db->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    function ver_seriadas(){
        $sql = "select m.id, m.clave,m.nombre,m.laboratorio_edo,m.horas_teoricas,m.horas_practicas,m.creditos,m.semestre, ms.nombre as seriada_con,dm.division_materia, c.nombre as campo_pertenesiente from materias m left join campo c on m.pertenece_a_campo = c.id left join division_materia dm on m.tipo_materia = dm.id left join materias ms on ms.id = m.materia_seriada where m.materia_seriada is not null order by m.id asc";

        $stmt = $this->db->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

}
