create or replace function to_upper_divi() RETURNS trigger as $$
    begin
        new.division_materia = upper(new.division_materia);
        return new;
    end;
    $$ language plpgsql;

create trigger to_upper_divi before insert or update on division_materia
    for each row execute procedure to_upper_divi();


create or replace function to_upper_materias() RETURNS trigger as $$
begin
    new.nombre = upper(new.nombre);
    return new;
end;
$$ language plpgsql;

create trigger to_upper_materias before insert or update on materias
    for each row execute procedure to_upper_materias();