create table if not exists division_materia
(
    id               serial primary key,
    division_materia varchar(40) not null
);


create table if not exists campo
(
    id     serial primary key,
    nombre varchar(40) not null
);



create table if not exists materias
(
    id                serial primary key,
    nombre            varchar(70) not null,
    clave             varchar(4)  not null unique,
    creditos          integer     not null,
    materia_seriada   integer references materias (id),
    semestre          integer     not null,
    laboratorio_edo   varchar(2) default null,
    horas_teoricas    integer     not null,
    horas_practicas   integer    default 0,
    tipo_materia      integer references division_materia (id),
    pertenece_a_campo integer references campo (id)
);

create table if not exists usuario
(
    id         serial primary key,
    num_cuenta varchar(9) unique not null,
    contraseña varchar(40)       not null
);

create table if not exists avance_usuario
(
    num_cuenta      varchar(9) references usuario (num_cuenta),
    materia_cursada integer references materias (id),
    calificacion    numeric(4, 2) default 5.0,
    primary key (num_cuenta, materia_cursada)
);
