select ms.clave,ms.nombre, m.nombre as seriada_antes
from materias m inner join materias ms on m.id=ms.materia_seriada
order by m.id asc;

select m.id,clave,m.nombre,creditos,
       laboratorio_edo,horas_teoricas,horas_practicas, c.nombre

from materias m join campo c on m.pertenece_a_campo = c.id
order by id,semestre asc;

select *
from materias where laboratorio_edo is not null;

select m.id, m.clave,m.nombre,m.laboratorio_edo,
       m.horas_teoricas,m.horas_practicas,
       m.creditos,m.semestre, ms.nombre as seriada_con,
       dm.division_materia, c.nombre
from materias m
    left join campo c on m.pertenece_a_campo = c.id
    left join division_materia dm on m.tipo_materia = dm.id
    left join materias ms on m.id = ms.materia_seriada;
