insert into division_materia(division_materia)
values ('ciencias básicas'),--1
       ('ciencias de la ingenieria'),--2
       ('ciencias sociales y humanidades'),--3
       ('unidad de alta tecnología'),--4
       ('ingeniería electrica'),--5
       ('ingeniería mecánica e industrial');--6

insert into campo(nombre)
values ('TRONCO COMÚN'),
       ('INGENIERÍA AERONÁUTICA'),
       ('INGENIERÍA ESPACIAL');



insert into materias(nombre, clave, creditos,
                     materia_seriada, semestre, laboratorio_edo,
                     horas_teoricas, horas_practicas, tipo_materia, pertenece_a_campo)
values ('álgebra', '1120', '1', null, 1, null, 4, 0, 1, 1);

insert into materias(nombre, clave, creditos,
                     materia_seriada, semestre, laboratorio_edo,
                     horas_teoricas, horas_practicas, tipo_materia, pertenece_a_campo)
values ('cálculo y geometría analítica', '1121', 12, null, 1, null, 6, 0, 1, 1);
insert into materias(nombre, clave, creditos,
                     materia_seriada, semestre, laboratorio_edo,
                     horas_teoricas, horas_practicas, tipo_materia, pertenece_a_campo)
values ('química', '1123', 10, null, 1, 'L+', 4, 0, 1, 1);
insert into materias(nombre, clave, creditos,
                     materia_seriada, semestre, laboratorio_edo,
                     horas_teoricas, horas_practicas, tipo_materia, pertenece_a_campo)
values ('FUNDAMENTOS DE FÍSICA', '1130', 6, null, 1, 'L+', 2, 2, 1, 1);
insert into materias(nombre, clave, creditos,
                     materia_seriada, semestre, laboratorio_edo,
                     horas_teoricas, horas_practicas, tipo_materia, pertenece_a_campo)
values ('INTRODUCCIÓN A LA INGENIERÍA AEROESPACIAL', '8888', 4, null, 1, null, 2, 0, 4, 1);
insert into materias(nombre, clave, creditos,
                     materia_seriada, semestre, laboratorio_edo,
                     horas_teoricas, horas_practicas, tipo_materia, pertenece_a_campo)
values ('REDACCIÓN Y EXPOSICIÓN DE TEMAS DE INGENIERÍA', '1124', 6, null, 1, null, 2, 2, 3, 1);

insert into materias(nombre, clave, creditos,
                     materia_seriada, semestre, laboratorio_edo,
                     horas_teoricas, horas_practicas, tipo_materia, pertenece_a_campo)
values ('ÁLGEBRA LINEAL', '1220', 8, (select id from materias where clave = '1120'), 2, null, 4, 0, 1, 1);
insert into materias(nombre, clave, creditos,
                     materia_seriada, semestre, laboratorio_edo,
                     horas_teoricas, horas_practicas, tipo_materia, pertenece_a_campo)
values ('CÁLCULO INTEGRAL', '1221', 8, (select id from materias where clave = '1121'), 2, null, 4, 0, 1, 1);
insert into materias(nombre, clave, creditos,
                     materia_seriada, semestre, laboratorio_edo,
                     horas_teoricas, horas_practicas, tipo_materia, pertenece_a_campo)
values ('ESTÁTICA', '1223', 8, (select id from materias where clave = '1121'), 2, null, 4, 0, 1, 1);
insert into materias(nombre, clave, creditos,
                     materia_seriada, semestre, laboratorio_edo,
                     horas_teoricas, horas_practicas, tipo_materia, pertenece_a_campo)
values ('MEDIO AMBIENTE AEROESPACIAL', '8889', 4, null, 2, null, 2, 0, 4, 1);
insert into materias(nombre, clave, creditos,
                     materia_seriada, semestre, laboratorio_edo,
                     horas_teoricas, horas_practicas, tipo_materia, pertenece_a_campo)
values ('FUNDAMENTOS DE PROGRAMACIÓN', '1122', 10, null, 2, 'L', 4, 2, 5, 1);
insert into materias(nombre, clave, creditos,
                     materia_seriada, semestre, laboratorio_edo,
                     horas_teoricas, horas_practicas, tipo_materia, pertenece_a_campo)
values ('DIBUJO INDUSTRIAL', '8890', 8, null, 2, 'L', 2, 4, 4, 1);

insert into materias(nombre, clave, creditos,
                     materia_seriada, semestre, laboratorio_edo,
                     horas_teoricas, horas_practicas, tipo_materia, pertenece_a_campo)
values ('FUNDAMENTOS DE PROBABILIDAD Y ESTADÍSTICA', '8891', 8, (select id from materias where clave = '1220'), 3, null,
        4, 0, 1, 1);
insert into materias(nombre, clave, creditos,
                     materia_seriada, semestre, laboratorio_edo,
                     horas_teoricas, horas_practicas, tipo_materia, pertenece_a_campo)
values ('CÁLCULO VECTORIAL', '1321', 8, (select id from materias where clave = '1221'), 3, null, 4, 0, 1, 1);
insert into materias(nombre, clave, creditos,
                     materia_seriada, semestre, laboratorio_edo,
                     horas_teoricas, horas_practicas, tipo_materia, pertenece_a_campo)
values ('ECUACIONES DIFERENCIALES', '1325', 8, (select id from materias where clave = '1221'), 3, null, 4, 0, 1, 1);
insert into materias(nombre, clave, creditos,
                     materia_seriada, semestre, laboratorio_edo,
                     horas_teoricas, horas_practicas, tipo_materia, pertenece_a_campo)
values ('CINEMÁTICA Y DINÁMICA', '1322', 8, (select id from materias where clave = '1223'), 3, null, 4, 0, 1, 1);
insert into materias(nombre, clave, creditos,
                     materia_seriada, semestre, laboratorio_edo,
                     horas_teoricas, horas_practicas, tipo_materia, pertenece_a_campo)
values ('TERMODINÁMICA', '1437', 10, null, 3, 'L+', 4, 2, 1, 1);
insert into materias(nombre, clave, creditos,
                     materia_seriada, semestre, laboratorio_edo,
                     horas_teoricas, horas_practicas, tipo_materia, pertenece_a_campo)
values ('CULTURA Y COMUNICACIÓN', '1222', 2, null, 3, null, 0, 2, 3, 1);

insert into materias(nombre, clave, creditos,
                     materia_seriada, semestre, laboratorio_edo,
                     horas_teoricas, horas_practicas, tipo_materia, pertenece_a_campo)
values ('ANÁLISIS DE SISTEMAS Y SEÑALES', '1443', 10, (select id from materias where clave = '1325'), 4, 'L+', 4, 2, 5,
        1);
insert into materias(nombre, clave, creditos,
                     materia_seriada, semestre, laboratorio_edo,
                     horas_teoricas, horas_practicas, tipo_materia, pertenece_a_campo)
values ('ELECTRICIDAD Y MAGNETISMO', '1414', 10, (select id from materias where clave = '1321'), 4, 'L+', 4, 2, 1, 1);
insert into materias(nombre, clave, creditos,
                     materia_seriada, semestre, laboratorio_edo,
                     horas_teoricas, horas_practicas, tipo_materia, pertenece_a_campo)
values ('ANÁLISIS NUMÉRICO', '1433', 8, (select id from materias where clave = '1325'), 4, null, 4, 0, 1, 1);
insert into materias(nombre, clave, creditos,
                     materia_seriada, semestre, laboratorio_edo,
                     horas_teoricas, horas_practicas, tipo_materia, pertenece_a_campo)
values ('MATEMÁTICAS AVANZADAS', '1424', 8, null, 4, null, 4, 0, 1, 1);
insert into materias(nombre, clave, creditos,
                     materia_seriada, semestre, laboratorio_edo,
                     horas_teoricas, horas_practicas, tipo_materia, pertenece_a_campo)
values ('FUNDAMENTOS DE MECÁNICA DE VUELO', '8892', 6, (select id from materias where clave = '1322'), 4, null, 3, 4, 4,
        1);

insert into materias(nombre, clave, creditos,
                     materia_seriada, semestre, laboratorio_edo,
                     horas_teoricas, horas_practicas, tipo_materia, pertenece_a_campo)
values ('ANÁLISIS DE CIRCUITOS ELÉCTRICOS', '1592', 10, (select id from materias where clave = '1443'), 5, 'L+', 4, 2,
        5, 1);
insert into materias(nombre, clave, creditos,
                     materia_seriada, semestre, laboratorio_edo,
                     horas_teoricas, horas_practicas, tipo_materia, pertenece_a_campo)
values ('TEORÍA ELECTROMAGNÉTICA', '0879', 10, (select id from materias where clave = '1414'), 5, 'L+', 4, 2, 5, 1);
insert into materias(nombre, clave, creditos,
                     materia_seriada, semestre, laboratorio_edo,
                     horas_teoricas, horas_practicas, tipo_materia, pertenece_a_campo)
values ('MECÁNICA DE FLUIDOS I', '0462', 10, null, 5, 'L+', 4, 2, 6, 1);
insert into materias(nombre, clave, creditos,
                     materia_seriada, semestre, laboratorio_edo,
                     horas_teoricas, horas_practicas, tipo_materia, pertenece_a_campo)
values ('INGENIERÍA DE MATERIALES', '1570', 10, null, 5, 'L+', 4, 2, 6, 1);
insert into materias(nombre, clave, creditos,
                     materia_seriada, semestre, laboratorio_edo,
                     horas_teoricas, horas_practicas, tipo_materia, pertenece_a_campo)
values ('DERECHO AÉREO Y ESPACIAL', '8893', 6, null, 5, null, 3, 0, 4, 1);
insert into materias(nombre, clave, creditos,
                     materia_seriada, semestre, laboratorio_edo,
                     horas_teoricas, horas_practicas, tipo_materia, pertenece_a_campo)
values ('INTRODUCCIÓN A LA ECONOMÍA', '1413', 8, null, 5, null, 4, 0, 3, 1);

insert into materias(nombre, clave, creditos,
                     materia_seriada, semestre, laboratorio_edo,
                     horas_teoricas, horas_practicas, tipo_materia, pertenece_a_campo)
values ('DISPOSITIVOS Y CIRCUITOS ELECTRÓNICOS', '1618', 10, (select id from materias where clave = '1443'), 6, 'L+', 4,
        2, 4, 1);
insert into materias(nombre, clave, creditos,
                     materia_seriada, semestre, laboratorio_edo,
                     horas_teoricas, horas_practicas, tipo_materia, pertenece_a_campo)
values ('SISTEMAS DE COMUNICACIONES', '1686', 8, null, 6, 'L+', 3, 2, 4, 1);
insert into materias(nombre, clave, creditos,
                     materia_seriada, semestre, laboratorio_edo,
                     horas_teoricas, horas_practicas, tipo_materia, pertenece_a_campo)
values ('MODELADO DE SISTEMAS FÍSICOS', '0508', 8, null, 6, null, 4, 0, 6, 1);
insert into materias(nombre, clave, creditos,
                     materia_seriada, semestre, laboratorio_edo,
                     horas_teoricas, horas_practicas, tipo_materia, pertenece_a_campo)
values ('INGENIERÍA TÉRMICA', '8894', 8, (select id from materias where clave = '1437'), 6, null, 4, 0, 4, 1);
insert into materias(nombre, clave, creditos,
                     materia_seriada, semestre, laboratorio_edo,
                     horas_teoricas, horas_practicas, tipo_materia, pertenece_a_campo)
values ('MECÁNICA DE SÓLIDOS', '1540', 6, null, 6, null, 4, 0, 6, 1);
insert into materias(nombre, clave, creditos,
                     materia_seriada, semestre, laboratorio_edo,
                     horas_teoricas, horas_practicas, tipo_materia, pertenece_a_campo)
values ('ÉTICA PROFESIONAL', '1052', 6, null, 6, null, 2, 2, 3, 1);

insert into materias(nombre, clave, creditos,
                     materia_seriada, semestre, laboratorio_edo,
                     horas_teoricas, horas_practicas, tipo_materia, pertenece_a_campo)
values ('DISEÑO DIGITAL', '1617', 10, null, 7, 'L+', 4, 2, 5, 1);
insert into materias(nombre, clave, creditos,
                     materia_seriada, semestre, laboratorio_edo,
                     horas_teoricas, horas_practicas, tipo_materia, pertenece_a_campo)
values ('FUNDAMENTOS DE SISTEMAS ELECTRÓNICOS ANALÓGICOS', '8896', 10, (select id from materias where clave = '1618'),
        7, 'L+', 4, 2, 4, 1);
insert into materias(nombre, clave, creditos,
                     materia_seriada, semestre, laboratorio_edo,
                     horas_teoricas, horas_practicas, tipo_materia, pertenece_a_campo)
values ('CONTROL AUTOMÁTICO', '0551', 8, (select id from materias where clave = '0508'), 7, null, 4, 0, 6, 1);
insert into materias(nombre, clave, creditos,
                     materia_seriada, semestre, laboratorio_edo,
                     horas_teoricas, horas_practicas, tipo_materia, pertenece_a_campo)
values ('AERODINÁMICA', '8897', 6, (select id from materias where clave = '0462'), 7, null, 3, 0, 4, 1);
insert into materias(nombre, clave, creditos,
                     materia_seriada, semestre, laboratorio_edo,
                     horas_teoricas, horas_practicas, tipo_materia, pertenece_a_campo)
values ('VIBRACIONES', '8898', 6, null, 7, null, 3, 0, 4, 1);
insert into materias(nombre, clave, creditos,
                     materia_seriada, semestre, laboratorio_edo,
                     horas_teoricas, horas_practicas, tipo_materia, pertenece_a_campo)
values ('EVALUACIÓN DE PROYECTOS DE INVERSIÓN', '1955', 8, null, 7, null, 4, 0, 6, 1);

insert into materias(nombre, clave, creditos,
                     materia_seriada, semestre, laboratorio_edo,
                     horas_teoricas, horas_practicas, tipo_materia, pertenece_a_campo)
values ('FUNDAMENTOS DE ANTENAS Y SISTEMAS DE RADIOTRANCEPTORES', '8899', 8,
        (select id from materias where clave = '1686'), 8, 'L+', 3, 2, 4, 1);
insert into materias(nombre, clave, creditos,
                     materia_seriada, semestre, laboratorio_edo,
                     horas_teoricas, horas_practicas, tipo_materia, pertenece_a_campo)
values ('TRANSFERENCIA DE CALOR', '1860', 10, null, 8, 'L+', 4, 2, 6, 1);
insert into materias(nombre, clave, creditos,
                     materia_seriada, semestre, laboratorio_edo,
                     horas_teoricas, horas_practicas, tipo_materia, pertenece_a_campo)
values ('AVIÓNICA I', '8900', 6, null, 8, null, 3, 0, 4, 1);
insert into materias(nombre, clave, creditos,
                     materia_seriada, semestre, laboratorio_edo,
                     horas_teoricas, horas_practicas, tipo_materia, pertenece_a_campo)
values ('SISTEMAS DE PROPULSIÓN', '8901', 6, null, 8, null, 3, 0, 4, 1);
insert into materias(nombre, clave, creditos,
                     materia_seriada, semestre, laboratorio_edo,
                     horas_teoricas, horas_practicas, tipo_materia, pertenece_a_campo)
values ('ESTRUCTURAS AEROESPACIALES', '8902', 6, null, 8, null, 3, 0, 4, 1);
insert into materias(nombre, clave, creditos,
                     materia_seriada, semestre, laboratorio_edo,
                     horas_teoricas, horas_practicas, tipo_materia, pertenece_a_campo)
values ('DESARROLLO DE EMPRENDEDORES', '8903', 8, null, 8, null, 3, 2, 4, 1);

insert into materias(nombre, clave, creditos,
                     materia_seriada, semestre, laboratorio_edo,
                     horas_teoricas, horas_practicas, tipo_materia, pertenece_a_campo)
values ('MODELADO BASADO EN DISEÑO', '8904', 10, (select id from materias where clave = '0508'), 9, 'L', 2, 6, 4, 1);
insert into materias(nombre, clave, creditos,
                     materia_seriada, semestre, laboratorio_edo,
                     horas_teoricas, horas_practicas, tipo_materia, pertenece_a_campo)
values ('RECURSOS Y NECESIDADES DE MÉXICO', '2080', 8, null, 9, null, 4, 0, 3, 1);

insert into materias(nombre, clave, creditos,
                     materia_seriada, semestre, laboratorio_edo,
                     horas_teoricas, horas_practicas, tipo_materia, pertenece_a_campo)
values ('AEROELASTICIDAD', '8905', 6, null, 9, null, 3, 0, 4, 2);

insert into materias(nombre, clave, creditos,
                     materia_seriada, semestre, laboratorio_edo,
                     horas_teoricas, horas_practicas, tipo_materia, pertenece_a_campo)
values ('AERONAVES NO TRIPULADAS', '8906', 6, null, 10, null, 3, 0, 4, 3);



insert into usuario (num_cuenta, contraseña)
values ('313160282', md5(1234)),
       ('312342353', md5(1234)),
       ('324356342', md5(1234)),
       ('412354364', md5(1234)),
       ('314366523', md5(1234));
