<?php
class Usuario{
    private $db;

    function __construct(){
        $this->db=new Conexion();
    }

    function get_usuario($cuenta){
        $sql = "select num_cuenta from usuario where num_cuenta = :cuenta";
        $stmt = $$this->db->prepare($sql);
        $stmt->bindParam(':cuenta', $cuenta);
        $stmt->execute();
        if($stmt == true){
            $idReturn = $stmt->fetch(PDO::FETCH_ASSOC);
            return $idReturn;
        } else {
            return false;
        }
    }

    function get_pass($cuenta){
        $sql = "select contraseña from usuario where num_cuenta = :cuenta";
        $stmt = $$this->db->prepare($sql);
        $stmt->bindParam(':cuenta', $cuenta);
        $stmt->execute();
        if($stmt == true){
            $idReturn = $stmt->fetch(PDO::FETCH_ASSOC);
            return $idReturn;
        } else {
            return false;
        }
    }

    function ver_cuenta($u,$p){
        $sql = "select num_cuenta, contraseña from usuario where num_cuenta=:u and contraseña=:p";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':u', $u);
        $stmt->bindParam(':p', $p);
        $stmt->execute();
        if($stmt == true){
            return $stmt->fetch(PDO::FETCH_ASSOC);
        } else {
            return false;
        }
    }
}
