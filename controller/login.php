<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
require_once "../model/Conexion_BD.php";
require_once "../model/Usuario.php";


session_start();

if(isset($_SESSION['num_cuenta'])) {
	header('location:view/panel.html');
}
$Usuario = new Usuario();
$username = $_POST['inputEmail'];
$passw = md5($_POST['inputPassword']);
// $pw = md5($passw);
$uid = $Usuario->ver_cuenta($username, $passw);
// var_dump($uid);
if ($uid) {
    $_SESSION['num_cuenta'] = $uid['num_cuenta'];
    // echo json_encode(array('error' => false, 'es_admin' => $uid['admin'],
    // 'usuario_id' => $uid['usuario_id']));
    echo "success";
} else {
    echo json_encode(array('error' => true));
}