<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

session_start();



//require_once("model/Conexion_BD.php");
//require_once("controller/get_index.php");

if(!isset($_SESSION['num_cuenta'])){
    include_once 'controller/load_login.php';
}else{
    require_once 'controller/load_panel.php';
    if (!(isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == 'on' || 
                                       $_SERVER['HTTPS'] == 1) ||  
          isset($_SERVER['HTTP_X_FORWARDED_PROTO']) &&   
          $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https'))
    {
        $redirect = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];   
        exit();
    }
}
?>
